from flask_security.forms import RegisterForm, Required
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired, NumberRange


# переопределяем форму регистрации встовляем нужные поля
class ExtendRegisterForm(RegisterForm):
    block = StringField('block', [DataRequired()])
    # role_id = IntegerField('role', [NumberRange(min=1, max=30, message='пожалуйста укажите id присваеваемой роли (от 1 до 30)')])
