from flask import Flask, url_for, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_security import SQLAlchemyUserDatastore, Security
from flask_admin import Admin, helpers
from flask_login import LoginManager
from views import MyModelView, CustomView, UserView
from registry_form import *

from config import Config
from database.models import User, Role, Permission
from database.connected import db_conn


def create_app():
    applic = Flask(__name__)
    applic.config.from_object(Config)
    applic.config.from_pyfile('config-extended.py')

    # Регистрация путей Blueprint
    from routes import admin_bp, main_bp
    applic.register_blueprint(admin_bp, url_prefix="/admin")
    applic.register_blueprint(main_bp, url_prefix="/main")

    @admin_bp.app_context_processor
    @main_bp.app_context_processor
    def inject_permissions():
        '''Использовать контекстный процессор, переменные доступны в глобальном шаблоне'''
        return dict(Permission=Permission)
    return applic


app = create_app()
db = SQLAlchemy(app)
db.create_all(db_conn)
migrate = Migrate(app, db)

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore, register_form=ExtendRegisterForm)

# Create admin
admin = Admin(
    app,
    'Yamata-ADMIN',
    # index_view=MyAdminIndexView(),
    base_template='admin/master-extended.html',
    template_mode='bootstrap4'
)
# Add view
admin.add_view(UserView(User, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name="Users"))
admin.add_view(MyModelView(Role, db.session, menu_icon_type='fa', menu_icon_value='fa-server', name="Roles"))
admin.add_view(CustomView(name="Custom view", endpoint='custom', menu_icon_type='fa', menu_icon_value='fa-connectdevelop',))

# define a context processor for merging flask-admin's template context into the
# flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=helpers,
        get_url=url_for
    )

# Ставим редирект, если пользователь не авторизован, для страниц где обязательна авторизация
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'admin_blueprint.login'
# проверка на свежесть токена
login_manager.refresh_view = "accounts.reauthenticate"
login_manager.needs_refresh_message = (
    u"To protect your account, please reauthenticate to access this page."
)
login_manager.needs_refresh_message_category = "info"
# чтобы предотвратить кражу сеансов ваших пользователей
# strong - Если в режиме непостоянной сессии идентификаторы не совпадают , то вся сессия
# (а также запоминающий токен, если он существует) удаляется
# basic - Если идентификаторы не совпадают в basic режиме или когда сеанс является
# постоянным, то сеанс будет просто помечен как несвежий, и все,
# что требует нового входа в систему, заставит пользователя повторно аутентифицироваться
login_manager.session_protection = "strong"

# Отвечает за сессию пользователей. Запрещает доступ к роутам, перед которыми указано @login_required
@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(user_id)

@app.route('/')
def index():
    return render_template('index.html')

